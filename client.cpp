#include <boost/asio.hpp>
#include <iostream>

using namespace boost;

int     main(void)
{
	//step1.    assume that client app has already obtained the IP and Port Number
	std::string					raw_ip_address = "127.0.0.1";
	unsigned short				port_num = 3333;
	

	//used to store info about err that happens while parsing raw IPaddr
	boost::system::error_code	ec;
	
	
	//step2.    using IP protocol version independent addr representation
	asio::ip::address			ip_address = asio::ip::address::from_string(raw_ip_address, ec);
	if (ec.value() != 0)
	{
		std::cout << "Failed to parse the IP address. Err code = ";
		std::cout << ec.value() << ". Message: " << ec.message() << std:endl;
		return ec.value();
	}
	

	//step3.
	asio::ip::tcp::endpoint		ep(ip_address, port_num);
	

	//step4.    the endpoint is ready and can be used to specify a particular server
	//          in the network the client wants to communicate with
	return 0;
}
