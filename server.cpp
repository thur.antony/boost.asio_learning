#include <boost/asio.hpp>
#include <iostream>

usign namespace boost

int     main(void)
{
	//step1.    here we assume that the server app has already obtained the protocol port number.
	unsigned short          port_num = 3333;
	
	
	//step2.    create special object of asio::ip::address class
	//          that specifies all IP-addresses available on the host.
	//          Note.   that we assume that server works over IPv6 protocol.
	asio::ip::address       ip_address = asio::ip::address_v6::any();
	
	
	//step3.
	asio::ip::tcp::endpoint ep(ip_address, port_num);
	
	
	//step4.    the endpoint is created and can be used to specify the IP address and port number
	//          on which the server app wants to listen for incoming connections.
	
	retrun 0;
}
